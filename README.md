# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Nightwatch test tools (http://nightwatchjs.org/)
* v1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Go to the folder using the terminal
* Do an **"npm install"**
* Write your test
* Update the **package.json** in order to run the command for your test
* Run your test using the name you declare on the package.json: **"npm run name_of_your_test"**