
var HtmlReporter  = require('nightwatch-html-reporter');
var moment        = require('moment');

var date = new Date();
var fileDate = moment(date).format('YYYY-MM-DD');
var fileTime = moment(date).format('HH-mm');
fileTime = fileTime.replace('-', 'h');

var reporter = new HtmlReporter({

  openBrowser: true,
  reportsDirectory: __dirname + '/reports/',
  reportFilename: '../../reports/' + fileDate + '_' + fileTime + '_Report.html',
  themeName: 'default',
  hideSuccess: false

});

module.exports = {
  write : function(results, options, done) {
    reporter.fn(results, done);
  }
};