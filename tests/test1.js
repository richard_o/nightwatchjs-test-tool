
// Adding the url for the test
var url = 'http://www.axa.com';

//On the console, showing the url
console.log(url);

module.exports = {

  // Start of the  test
  'Testing link AXA.COM' : function (browser) {
    browser
      .url(url)
      .waitForElementVisible('body', 1000)
      .end();
  }
  // End of the  test

  // If adding test, please put them below

};
